# CalisMT

CalisMT: https://gitlab.com/Calis-asso/CalisMT

A Green and Dark Theme for Mumble

Licence: MIT

Author: Association Calis - Adrien.V

Based on MetroMumble by xpoke https://github.com/xpoke/MetroMumble/releases

------------------------------------------------------

Installation on Linux:
- Unzip files in the Mumble Theme folder /home/user/.local/share/Mumble/Mumble/Themes/
- Mumble Settings > User Interface > Theme > Select Calis in the dropdown menu
- OK
- Restart the application

Note: OS X users should use the "OSX" qss files instead of the regular ones

Extras:
 - mumble.ico - Shortcut icon (by xPoke)
 - overlay.mumblelay - An overlay preset (Settings -> Overlay -> Load...) (by xPoke)
